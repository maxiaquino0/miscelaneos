import { Component, OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked,
          AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-ng-style></app-ng-style>

    <app-css></app-css>

    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem nisi quod deleniti optio. Hic commodi animi aspernatur,
        nam reiciendis earum excepturi asperiores officiis corrupti at mollitia ipsam cum quas? Doloremque!
    </p>

    <app-clases></app-clases>

    <hr>

    <p [appResaltado]="'orange'">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas veniam magni eius hic libero dicta fugiat quam molestiae dolore.
    </p>

    <br>
    <hr>
    <app-ng-switch></app-ng-switch>
  `,
  styles: []
})
export class HomeComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked,
  OnDestroy {

  constructor() {
    console.log('Constructor');
  }

  ngOnInit() {
    console.log('ngOnInit');
  }

  ngOnChanges() {
    console.log('ngOnChanges');
  }
  ngDoCheck() {
    console.log('ngDoCheck');
  }
  ngAfterContentInit() {
    console.log('ngAfterContentInit');
  }
  ngAfterContentChecked() {
    console.log('ngAfterContentChecked');
  }
  ngAfterViewInit() {
    console.log('ngAfterViewInit');
  }
  ngAfterViewChecked() {
    console.log('ngAfterViewChecked');
  }
  ngOnDestroy() {
    console.log('ngOnDestroy');
  }

}
