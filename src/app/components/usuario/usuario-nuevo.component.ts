import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-nuevo',
  template: `
    <p>
      usuario-nuevo works! -> {{idUsuario}}
    </p>
  `,
  styles: []
})
export class UsuarioNuevoComponent implements OnInit {

  idUsuario: string;

  constructor(private activatedRoute: ActivatedRoute) {
    console.log('Ruta Hija');
    this.activatedRoute.parent.params.subscribe(parametro => {
      console.log(parametro);
      this.idUsuario = parametro.id;
    });
  }

  ngOnInit() {
  }

}
